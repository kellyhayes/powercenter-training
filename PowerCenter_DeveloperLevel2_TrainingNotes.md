**[Module 1: Architecture]{.underline}**

-   Service Oriented Architecture (SOA)

    -   Each individual service runs as a service process on a server
        (called a node)

    -   Node could be Linux, UNIX, windows

    -   Communication is through TCP/IP (packets)

-   Informatica Domain -- a collection of nodes and services

    -   Administered from a browser based interface (called
        Administrator)

    -   Objects in the domain are stored in a repository called the
        domain database

-   Nodes

    -   All domains require at least one node

    -   A node corresponds to an operating system process that runs a
        Service manager

    -   Two types of nodes- gateway and worker

        -   Gateway -- ability to be the master node for the domain; a
            domain has one and only one master gateway node (would also
            have backup gateway nodes for the case when the master node
            fails)

        -   Worker -- any node not configured as a gateway

    -   Every node has an xml file on the node hard drive that tells the
        node info it needs to communicate with other nodes on the domain
        (i.e. hostname, listening ports)

-   Service Manager

    -   If this is not running on a node for any reason, then the node
        is not on the domain and cannot run any services

    -   If this is running on a node, then any service can be run on the
        node as long as the node is configured to run that service

    -   Service manager is also responsible for
        authentication/authorization

    -   Also responsible for logging

        -   Logs are accessible from the administrator tool

    -   License objects

        -   Can be applied to a service on a node

        -   Does not physically live on a node

        -   Exists as metadata in the domain database ( so you do NOT
            need a license object for each node)

-   Application Services

    -   Provide the functionalities of the Informatica Platform

    -   Configured in the administrator tool

    -   External clients interact with these services via nodes

    -   Examples: PC Repository Service, PC Integration Service, Data
        Integration Service, Analyst Service, etc.

-   Gateway nodes can run services just like worker nodes

-   PowerCenter Repository Service

    -   Stores metadata on sources, targets, mappings, etc.

    -   Connects to the PC client tools

    -   With a high availability license, RS can be configured to run on
        more than one node at a time

    -   Each service connected to a PC RS database (must be for the
        service to ruN)

-   PowerCenter Integration Service

    -   The workhorse that runs the PC mappings, workflows, etc.

    -   Does the heavy lifting

    -   Every PC RS must have at least one PC IS (common to have more
        than one IS for an RS)

-   Reporting Service is optional

-   Web Service is also optional

-   Connection Management

    -   Done from workflow manager \> Connections menu

    -   Can set permissions, create/view/edit connections

-   Domain Administration is created during installation (admin for
    Repository, Integration Service, Reporting Service) but can assign
    other administrators later for nodes, services, folders, etc.

-   High Availability License

    -   Option to run a service as primary service on a primary node and
        backup service on a backup node so that if failure happens, it
        will run on the backup

    -   "Automatic failover restart recovery"

    -   Backup node must have the resources necessary to run these
        services (connections, binary files, etc.)

-   Without that license, there is manual recovery option

    -   Additional options as well without the HA product

    -   Internal PowerCenter and repository database resilience

    -   Can configure the informatica Services to restart after failure

    -   Can configure multiple gateway nodes

    -   Manual recovery of workflows and sessions

-   Can determine the resilience timeout using command line:

    -   -timeout, -t

    -   Or INFA\_CLIENT\_RESILIENCE\_TIMEOUT environment variable

**[Module 2: Parameter Files]{.underline}**

-   Parameter Files -- used to set values of Mapping parameters and
    variables

    -   Mapping parameter -- a value that will not change while a
        session is running, but that might change after a session has
        completed

        -   Can be applied to workflow, worklet and session levels

        -   Cannot be associated with a mapping, but can be associated
            with a session that runs a mapping

    -   To set these...

        -   For workflow: workflows \> Edit \> Properties tab

        -   For session: session task \> edit \> properties tab

        -   Unnecessary to set the parameter file at both levels!! Pick
            one.

            -   If you choose workflow level -- less files, but larger
                files

            -   If you choose session level -- more files, but smaller
                files

        -   Can also do this from command line if desired (pmdcmd
            -paramfile)

-   Structure

    -   ![](media/image1.png){width="5.10294072615923in"
        height="2.8916666666666666in"}

-   Especially helpful to use these files with migration

    -   Migration -- production to dev, dev to QA, etc.

    -   This way if logs and directories and things are different in
        each environment, you can just set them in the parameter file
        instead of changing the workflows, etc.

-   One of the most useful things to parameterize -- database connection
    objects!

    -   Don't use source or target in the names of these variables! The
        same connections can be used for both targets and sources, no
        need to delineate

-   Session and workflow logs will always indicate which parameter file
    was used

-   Also, possible to parameterize an entire expression value

    -   Not that common, but if logic might change, it is possible and
        potentially useful to do this

    -   To do so, must set datatype to string and make sure the
        precision is set long enough to hold the entire expression.
        Also, isExprVar must be set to true.

    -   Disadvantage- logic is removed from mapping, which makes
        mappings harder to understand and maintain

-   Troubleshooting parameter files

    -   Check the filename and path

    -   Check the information in the header of the file (typos in header
        means everything under that header is ignored)

-   Problem: When moving from dev to production how do you maintain all
    appropriate DB connections within each session?

    -   In a Dev workflow, the sessions inside the workflow connect to
        dev databases

    -   In a prod workflow, the sessions inside the workflow connect to
        prod databases

    -   So how to fix this and maintain all appropriate connections?

        -   User defined connection variables in conjunction with a
            parameter file

        -   \$DBConnection followed by a descriptive name (naming
            convention)

            -   Don't use source or target in the name!

            -   Don't use prod or dev in the name! We are using this
                variable in both environments, so don't delineate!

        -   EXAMPLE: ![](media/image2.png){width="3.65701334208224in"
            height="2.2868055555555555in"}

        -   Remember to set the database connection object permissions
            correctly!

        -   Don't confuse these user-defined connection variables with
            the built in connection variables- \$Source and \$Target-
            these are used specifically for lookups!!

-   NOTE: with parameters, the difference between \$ and \$\$ is-

    -   \$ is used for internal parameters/variables (i.e. DBConnection
        or PMSessionLogDir

    -   \$\$ is used for user-defined parameters/variables

        -   \$\$ also used when it's a mapping level (rather than
            session?)

    -   <https://stackoverflow.com/questions/19564777/what-is-the-difference-between-informatica-parameters>

    -   Might not actually matter as long as the variables match
        (between file and defined variable/parameter in the workflow,
        mapping, etc.)

-   NOTE: Parameters and Parameter names are case sensitive!!

**[Module 3: User-Defined and Advanced Functions]{.underline}**

-   Informatica has a built in transformation language

    -   SQL like, but not derived from any particular database

-   Data Security

    -   PC has some encryption/decryption functions included

    -   When PC encrypts a string, the encrypted data can be written to
        a flat file or directly to a database

    -   2 levels of encryption offered --

        -   BASE64- 64 bit encryption

        -   AES (Advanced encryption standard)- 128 bit encryption

    -   Will not protect data from dedicated hackers, but will help with
        contractors/more laypeople

-   Financial functions

    -   Time value of money functions

        -   NPER

        -   PMT

        -   PV

        -   RATE

-   Character functions

    -   REPLACECHR and REPLACESTR allow you to replace a character or
        string within another string

    -   LEAST and GREATEST will compare a list of values and return the
        highest or lowest (can do this with alphabetical order,
        numerical order, or dates)

        -   Different from MAX and MIN -- these look across a bunch of
            records

        -   LEAST and GREATEST look at one record at a time (looks at
            values within one record, so a least or greatest value will
            be returned for each record)

-   Data cleansing functions

    -   REG\_MATCH -- uses a regular expression and returns T/F if it
        matches

    -   REG\_EXTRACT -- uses the regular expression to return the part
        of the input string that matches the regular expression

    -   REG\_REPLACE -- uses a regular expression to replace a part of
        the input string

-   String functions

    -   INDEXOF -- examines a comma delimited list of values supplied by
        one record and return the position of the string you're looking
        for

    -   CHOOSE -- does the exact opposite \^ takes a comma delimited
        list of values and a position and the function returns the value
        at that position

    -   REVERSE -- reverses what you provide (cat -\> tac)

-   User-defined functions

    -   Operate on the data one record at a time, so you cannot use
        aggregate functions when creating these

    -   Scope of these is one folder, so can only be used on entities
        within that folder

    -   Don't support the use of shortcuts

    -   Two types

        -   Public -- callable from any transformation expression

        -   Private -- only callable from another user-defined function
            (cannot use it directly in the mapping)

        -   By default, they are private

-   Creating user-defined functions...

    -   Designer \> Tools \> User-defined functions \> New

    -   Can also right click on the user-defined functions sub-folder
        and click New

    -   Argument names do not need to match ports or anything!! Name the
        arguments in a generic way so that the function can be used in
        many contexts, etc.

    -   Must set a function to "Public" if you want to be able to use it
        in an expression!!

    -   :UDF. tells PC you are about to use a user-defined function

    -   If you want to make a copy of a user-defined function (remember,
        you can't create shortcuts),

        -   You can copy an object that uses the user-defined function,
            the function will come with the object automatically

        -   You can export the user-defined function as an XML and then
            import it somewhere else (Repository \> Import objects)­­­

-   NOTE: Encryption using the AES functions outputs binary, but if the
    target table doesn't support binary, you need to wrap the AES
    function in a BASE64 function because BASE64 outputs an alphanumeric
    string

    -   Example: ENC\_BASE64(AES\_ENCRYPT(Name, \$EncryptKey))

**[Module 4: Pivoting Data]{.underline}**

-   **Normalizer** -- turns one row into many

    -   ![](media/image3.png){width="6.5in"
        height="3.479861111111111in"}

    -   Naming convention -- NRM\_

    -   Ports -- view the transformation ports and attributes

    -   Properties- configure the tracing level for the session log file
        and choose to reset or restart the generated key sequence value

    -   Normalizer -- define the structure of the source data as columns
        and groups of columns

-   Occurs = the ratio of records into the normalizer transformation to
    the number of records coming out of the normalizer transformation

    -   Occurs in the example above is 4

    -   For every record going in, 4 records are coming out (one for
        each week)

-   Normalizer GK = generated key

    -   The integration service increments the generated key sequence
        number each time it processes a source row

    -   In the above example, a GK would be created for Bob first (GK
        = 1) then for Tina (GK = 2). There would be a new GK generated
        for each REP.

-   Normalizer GCID = generated column IDs

    -   The transformation returns a generated column ID for each
        instance of a multiple occurring field

    -   In the example above, a GCID would be created for week 1, then
        week 2, etc. and reset itself at a new GK. So bob would have a
        GCID of 1,2,3 and 4, one for each week. Then Tina would have a
        GCID of 1,2,3 and 4 ...

-   ![](media/image4.png){width="5.2325185914260715in"
    height="2.286431539807524in"}

-   **Denormalizer** -- turn many rows into one

    -   Use an aggregator transformation (there is no "denormalizer"
        transformation in PC)

-   Must configure the aggregator transformation appropriately to use it
    as a denormalizer

    -   Group by port is the repeating port

    -   ![](media/image5.png){width="5.3288156167979in"
        height="2.204968285214348in"}

    -   To reverse the normalization done above (with Bob and Tina)
        \^\^\^\^

**[Module 5: Dynamic Lookups]{.underline}**

-   Uses a regular lookup transformation, but with the dynamic lookup
    cache enabled. (there is no "dynamic lookup" transformation)

-   All lookup transformations cache their lookup data by default.
    Typically, this data in the cache is static (no change during the
    session).

-   If the lookup table is the target table, the cache is changed
    dynamically as target load rows are processed.

    -   Want to use update else insert logic (if the record exists,
        update, else insert).

    -   If the cache is not set to dynamic, then if a record is inserted
        during the session, the cache will not update and then if that
        record comes through again there will be no match and the record
        will be inserted AGAIN when it should be updated (this is why we
        **turn on dynamic cache for situations when the lookup table is
        also the target table**).

    -   New rows to be inserted or updated in the target are also
        written to the cache

    -   ![](media/image6.png){width="3.2245833333333334in"
        height="1.5692311898512685in"}

-   Dynamic Lookup Cache Ports

    -   "NewLookupRow" -- flag that denotes how each record coming out
        of the lookup transformation affected the cache

        -   0 -- record did not affect cache

        -   1 -- record was inserted into cache

        -   2 -- record was used to update the cache

    -   "Associated Expression" -- used to indicate which output ports
        of the lookup are associated with which input ports of the
        target (they all have to be associated because the lookup and
        the target are the same table)

        -   You are allowed to enter an expression here that derives the
            value PC would use to insert or update

    -   "Ignore Null Inputs" -- normally not checked. If you leave it
        unchecked, then NULLS are treated as usual where they will
        overwrite the existing values. If you check this box, then PC
        will preserve the current values and not update them if the new
        record coming in has NULLs.

    -   "Ignore in Comparison" --

-   Dynamic Lookup Cache Properties

    -   "Synchronize Dynamic Cache" -- if you have two lookups running
        at the same time and this is checked, then when one instance of
        a reusable lookup transformation in dynamic cache mode, then the
        other lookup transformations will recognize that record as part
        of the cache and avoid duplicate work

    -   "Output Old Value on Update" -- allows you to do history
        tracking so that when something changes both the new and old
        value are recorded

    -   "Insert Else Update" -- if you leave this unchecked, when PC
        does not find a match in the cache it will insert the record,
        but when it does find a match in the cache it will do nothing.
        If you check this, then PC will insert a record when it doesn't
        find a match, but if it does find a lookup match in the cache it
        will use the new record to update the cache.

    -   "Update Else Insert" -- here for backwards compatibility. Only
        relevant if the "Insert Else Update" box is checked. If left
        unchecked, and PC finds a match in the cache, the record is not
        inserted. If you do check this box, when PC doesn't find a
        match, it inserts the record. If you do check this box and PC
        does find a match, it will update the record in the cache.

    -   ![](media/image7.png){width="5.257984470691164in"
        height="3.6654221347331584in"}

-   Rules of the dynamic lookup cache

    -   Does not handle deletions

    -   Not compatible with an unconnected lookup

    -   Lookup condition must be = (cant do \< or \>)

**[Module 6: Stored Procedure and SQL Transformations]{.underline}**

-   It is always better to use ordinary PC transformations to output
    data

    -   i.e. always better to use an aggregator transformation to
        aggregate data than to write a SQL statement to do the same

    -   PC transformations are more efficient and give better
        performance than SQL

    -   PC transformations are database neutral, so you don't have to
        worry about database specific SQL statements

    -   The integration service does not use SQL to transform data

-   Using SQL with PowerCenter (PC)...

    -   When might this be useful?

        -   To leverage complex legacy SQL or stored procedures (assists
            with transition from SQL as ETL to PC as ETL)

        -   To use algorithms such as looping

        -   To create tables from within a mapping

-   Stored Procedure Transformation -- passes records from the mapping
    to a stored procedure located on a database

    -   Passive transformation: returns only the first row of a result
        row of a result set

    -   The stored procedure must already exist. PowerCenter does not
        create the stored procedure

    -   Uses a database connection object to establish connectivity from
        the session to the stored procedure

    -   ![](media/image8.png){width="5.195096237970254in"
        height="1.5618591426071742in"}

-   Creating a Stored Procedure Transformation....

    -   Must choose an existing stored procedure or Oracle function

    -   You'll connect to a database then choose a stored procedure from
        the list

-   Each argument that the stored procedure accepts = an input port on
    the transformation

-   Each result that the stored procedure produces = an output port on
    the transformation

-   Unconnected Stored Procedure Transformation

    -   Very similar to using a lookup in unconnected mode

    -   Allows you to embed the stored procedure in a conditional
        statement in an expression to be called for only certain records

-   Stored Procedure "Type" Property

    -   Normal -- what we just talked about; processes data to and from
        stored procedure row-by-row

    -   Source Pre or Post Load -- stored procedure runs before or after
        data is read

    -   Target Pre or Post Load -- stored procedure runs before or after
        data is written

    -   **It is outdated to use these Source/target Pre/post load
        options!** The technique was created before workflows were an
        option. No longer a recommended course of action to do this.

    -   **Better choice is to create these as command tasks in the
        workflow!**

-   SQL Transformation -- runs SQL mid-stream in a mapping

    -   Script mode -- runs remote ANSI-SQL scripts in text files

        -   Can use a different script for each input row

        -   Passive; select statements ignored, one output row for each
            input row

    -   Query mode -- runs a query you provide in an editor within the
        transformation

        -   Active; select statements honored, can output multiple rows
            for each input row

    -   Will read in connection information as source data

-   Script mode ports

    -   Input ports = script file name with full path

        -   Additional input ports can be created if needed

    -   Output ports = result of running the script (result = passed or
        failed, scripterror = error if there is one)

        -   Additional output forts are not allowed

        -   No row data output

-   Query Mode / dynamic connection allows you to avoid having to create
    a large number of DB connection objects and instead feed in the
    connection information from a flat file row by row.

    -   ![](media/image9.png){width="5.231158136482939in"
        height="2.9810892388451444in"}

**[Module 7: Troubleshooting Methodology and Error
Handling]{.underline}**

-   Row Error Categories

    -   Transformation error -- data row has only passed partway through
        the mapping transformation logic

        -   An error occurs within a transformation

        -   By default Transformation error rows are logged in the
            session log

    -   Data reject -- database has rejected the record for a specific
        reason

        -   Data row is fully transformed according to the mapping logic

        -   Cannot be written to the target

        -   A data reject can be forced by an update strategy

        -   By default Data reject rows are put in the .bad file

-   Session Error Log Options

    -   Error Log type --

        -   If set to relational database, PC will automatically
            generate a set of error tables on a DB connection you
            specify. Will hold metadata about the errors, all data in
            the record that failed, etc.

        -   If set to file, will write out all of this info to a
            dedicated flat file.

-   Error handling strategies

    -   Data errors may require manual inspection, data correction or
        re-executing the process

    -   If data is critical, a notification process may need to be put
        in place

    -   A post-session e-main can send a message if an error is
        encountered

    -   Two main techniques

        -   Maintain database constraints

        -   Use mappings to trap data errors

            -   Route all data errors to fatal and non-fatal error
                tables. A view will be automatically generated to view
                the most recent errors

            -   Write erroneous records to flat files in the same data
                format that they arrived

**[Module 8: Transaction Processing]{.underline}**

-   Database transaction

    -   PowerCenter processes a bunch of records (default is 10,000) and
        loads them to the temporary cache

    -   Once that number hits, the records are actually committed and
        officially in the table

    -   Once the commit happens, the temporary cache is cleared out

    -   This \^\^\^ whole process is a DB transaction.

    -   Sometimes, a rollback needs to happen if a transaction fails
        which reverts the DB back to its original state

-   Transaction commit types

    -   Target-based (default) -- records are counted at the last active
        source transformation before the records are sent to the target
        DB

        -   Optimized for performance

    -   Source-based -- PC will count the records at the first active
        source transformation in the mapping (typically a SQ)

        -   Provides precise control over commit points

    -   User-defined -- using the data to set the commit point

        -   Provides for variable commit intervals driven by mapping
            logic and data

-   Target -- based commit

    -   Integration service commits rows based on the number of target
        rows

    -   Default is 10,000 rows

    -   Actual commit point depends on the following factors-

        -   Commit interval = the number of rows you want to use as a
            basis for commits

        -   Writer wait timeout -- if timeout is exceeded, commit is
            issued even if we haven't reached the 10,000 records.

            -   Set as a property of the integration service

            -   Default is 60 seconds

            -   Can be configured as 0 or a negative number

        -   Buffer blocks

            -   PC processes data as small memory blocks called buffers
                (typically \~ 100 records)

            -   When the buffer fills up, if the buffer contains a
                commit row, the data will get committed (even though the
                commit interval may not have been reached yet).

            -   Sometimes PC includes these commit rows to slightly
                improve its efficiency. It will do this when....

                -   The writer is idle for the amount of time specified
                    by the writer wait timeout interval OR

                -   The integration service fills a new bugger block and
                    reaches the commit interval OR

                -   We reach the end of the data stream

-   Target Load Order Group (TLOG)

    -   A set of one or more target definitions that gets its data from
        a single, common active source transformation

    -   A TLOG is one or more targets from a common SQ, Joiner or Union

    -   TLOGs only apply to mappings with more than one pipeline

    -   ![](media/image10.png){width="5.184615048118985in"
        height="3.073102580927384in"}

    -   Access the Target Load Order plan from the Mapping menu; Allows
        you to control the order the pipelines read their data

-   Source-based commit

    -   "Active source transformation" -- a transformation that does not
        preserve the order of the records (has the ability to shuffle
        the records or combine the records), such as:

        -   Aggregator, custom (configured as active), joiner,
            normalizer, rank, sorter, SQ

    -   "Commit source" -- the last active source transformation that
        the data comes out of before it reaches the relational target
        definition of the mapping

-   Constraint-based loading -- establishes the order in which the
    Integration Service loads individual targets from a single source
    qualifier

    -   If there is a PK and FK between two or more target definitions
        in a mapping, Constraint-based loading will ensure the PKs are
        loaded before the FKs so that no records are rejected for
        referencing NULLs

    -   Not every mapping supports constraint-based loading, though

    -   PC must know what is the PK and what is the FK so those things
        must be part of the metadata- that relationship must exist in
        the target definition

-   Constraint-based loading -- restrictions (AKA what a mapping needs
    to be able to support this)

    -   All targets must have a common active source

    -   Target tables must have key relationships defined within the
        PowerCenter repository metadata

    -   All targets must belong to the same target connection group

        -   If a session has more than one target connection group, it
            will process constraint-based loading separately for each
            target connection group

    -   Must treat rows as insert

-   Activate constraint-based loading with a checkbox at the session
    level

-   If you see that records were rejected because "parent key not found"
    then its likely a referential integrity problem, the FKs were loaded
    before PKs which caused an issue

    -   FIX = Constraint-based loading!!! Check that box and that
        problem will take care of itself

**[Module 9: Transaction Control Transformation]{.underline}**

-   Transaction in PowerCenter = a set of rows bound by commit or
    rollback rows

    -   All the records sent to the database between commit statements =
        the transaction

-   Transaction control transformations in a mapping make it so that the
    mapping does not use the commit rows, but instead an expression
    defines the commit and rollback criteria

    -   Session does not used a fixed commit interval -- instead uses
        the data in the mapping to set the commit points

-   Configured similarly to the update strategy transformation

-   Four flags set by the transaction control transformation

    -   1\. TC\_CONTINUE\_TRANSACTION -- the record will be written to the target
        database without a commit or rollback statement. This is the default.

    -   2\. TC\_COMMIT\_BEFORE -- record will first issue a commit statement,
        then be inserted to DB

    -   3\. TC\_COMMIT\_AFTER -- record will be inserted first, then a commit
        statement issued

    -   4\. TC\_ROLLBACK\_BEFORE -- rollback statement issued first then the
        record is inserted

    -   5\. TC\_ROLLBACK\_AFTER -- record will be inserted first then afterward a
        rollback statement will be issued

-   Why do this?

    -   It does *not* improve performance (in fact with smaller commit
        intervals, would make it worse)

    -   Do this for error handling! If you check "rollback transactions
        on errors" in the workflow properties tab, it will rollback
        everything if there's an error

        -   This means you can control that certain chunks of data are
            either fully committed in the DB or are not in the DB at all

        -   i.e. if you set TC\_COMMIT\_BEFORE every time a new invoice
            number comes in AND check that \^ checkbox, then each
            invoice number is either fully represented in the DB or
            isn't in the Db at all

-   If you wanted to include an aggregator transformation after
    including the transaction control transformation...

    -   If you did it normally, it would totally fuck up the transaction
        control stuff and revert to regular old fashioned target based
        commit intervals

    -   You have to change the transformation scope on the aggregator to
        "Transaction" instead of "All Input" so that it doesn't totally
        screw up the ordering and commit flags and things you set up

    -   Basically it will change the scope of the aggregator to a single
        DB transaction

**[Module 10: Recovery]{.underline}**

-   What are your options when workflows, mappings, etc. fail???

-   Recovery is the process of continuing workflows and tasks after they
    have been interrupted

    -   Session failures are more common than workflow failures

-   Think about a situation in which a session fails after many records
    have already been committed in the target DB

    -   Can't just re-run the session- you will end up with duplicate
        records

-   High availability recovery must be explicitly enabled on a workflow
    level (Workflow \> Properties)

    -   HA requires a specific license!!

    -   "Enable HA recovery" -- when checked, enabling workflow to run
        automatically in recovery mode if the Integration service has
        failed

    -   "Automatically recover terminated task" -- when checked, if an
        individual task in a workflow fails for any reason, the task
        will recover automatically

    -   In above context, automatically refers to no human interaction

-   Workflow State

    -   = information required by the Integration Service to determine
        where to begin workflow recovery, including:

        -   Which tasks had been completed

        -   Which tasks had been running

        -   Value of any workflow variables

    -   Normally this is stored in RAM

        -   Also included in a binary file on the local hard drive of
            the node running the IS running the workflow (stored in the
            \$PMStorageDir -- can find value in the Administrator tool)

        -   That way if there's a DB or network failure, you're Gucci

    -   Needed for recovery -- Integration Service needs to know where
        the workflow was at when it failed

    -   Can enable "suspend on error" in "Edit workflow" which makes it
        so that if any object in the workflow fails, the workflow goes
        into a suspended state

        -   Technically still running, but not doing anything anymore

-   Recovery Strategy -- allows you to configure how you want
    PowerCenter to react if a parti cular task fails

    -   Can set the recovery strategy differently for different sessions
        in the same workflow

    -   Fail task and continue workflow = default

    -   Restart task = will restart a workflow if it fails up to the max
        \# retries (which can be set)

    -   Resume from last checkpoint = recovery data used to avoid
        writing target data that has already been committed to the
        database

    -   Set this with a dropbox in the session properties

-   What if I don't have the HA license?

    -   Can recover "manually"

    -   Can resume suspended workflows manually

-   Recovery Right-click options

    -   "Restart workflow by recovering this task"

    -   When you right click the session, this \^ option will run the
        session in recovery mode with the recovery strategy you set

-   Cold Start option

    -   When you choose this in the right-click menu, you are restarting
        the session but NOT in recovery mode (essentially ignoring all
        that)

    -   This will maximize the time it takes to get all the data in the
        target table

-   Repeatable data

    -   When there is a need for recovery, you need to know if the data
        from the source is the same at the time of recovery as it was at
        the time of failure

        -   Deterministic output -- produces the same data (data at time
            of failure and recovery is the same)

        -   If the source is a flat file, and the same flat file is used
            at both times, a flat file is always deterministic and
            repeatable

    -   Also, is the data in the same order?

        -   Repeatable output -- produces data in the same order

-   Recovering "Automatically"

    -   Requires HA license

    -   Requires the workflow be pre-configured

    -   Requires that the backup node be available

    -   Manual recovery is still always an option

-   High availability license key adds these capabilities..

    -   Automatic service fail-over to a backup service running on a
        backup node

    -   Pre-configured workflow can recover without human intervention
        ("automatic" recovery)

-   Session state has data processed by the session -- only stored when
    "resume from last checkpoint" is set as the recovery strategy

    -   The data is cached in the storage directory (binary file)

    -   When the data is deterministic and repeatable -- full mode

        -   Full mode = the source data is re-read from the start. This
            does not require the data to be cached as part of the
            session state

    -   When the data is non-deterministic or non-repeatable --
        incremental mode

        -   Incremental mode = the integration service caches the source
            data so it does not have to be re-read during recovery

    -   Full or incremental mode is selected automatically by
        PowerCenter based upon the mapping and session characteristics

    -   There is a checkbox you can use to tell PowerCenter that data is
        deterministic or repeatable

        -   Source Qualifier \> Edit \> Properties

**[Module 11: Command Line Programs]{.underline}**

-   Command line programs, automatically included and executable

    -   Pmcmd -- manage workflows like start, stop, schedule

    -   Pmrep -- perform PowerCenter repository administration tasks

    -   Infacmd -- administer informatic application services

    -   Infasetup -- administer informatica domain and node properties

-   Why do we need these \^ when you can do most of that with the
    graphical interface?

    -   If you want things scripted for the future

-   Modes

    -   Command line mode -- can type in all commands from command line
        and put them in a script

    -   Interactive mode -- system can prompt you for each argument

-   Environment variables

    -   Optional for Unix or windows

    -   Configurable for command line programs

    -   On windows, configurable as user or system variables

    -   Examples-

        -   INFA\_DEFAULT\_DOMAIN -- stores the default domain name

        -   INFA\_DEFAULT\_DOMAIN\_PASSWORD -- stores the encrypted
            default user name password for the domain

**[Module 12: Performance Tuning Methodology]{.underline}**

-   Performance tuning objective = sessions to complete in as little
    amount of time as possible

-   Assumed that this methodology will be applied only to long sessions
    as it is impractical to apply this to every session in every
    workflow, etc.

-   An iterative process

    -   Establish benchmark

    -   Optimize memory

    -   Isolate bottleneck

    -   Tune bottleneck

    -   Take advantage of under-utilized CPU and memory

-   Bottleneck = whatever session is slowing down the workflow the most

-   Preliminary steps

    -   Override tracing level to terse or normal

        -   Override at session level to avoid having to examine each
            transformation in the mapping

        -   This property control the level of detail in the log

        -   Only use verbose tracing during development and only with
            very small data sets

-   Methodology

    -   1\. Find the worst bottleneck

    -   2\. Mapping design

    -   3\. Memory management

-   Most common bottleneck cause -- writing data to the database

    -   When a session runs, there are three parts: reading,
        transforming, writing

    -   NOTE: PowerCenter is designed in a way that all 3 \^ of these
        are happening at the same time, SO the slowest of the three will
        set the pace for the other two

-   Examine...

    -   Commit interval

    -   Constraint based load ordering (only want this checked if you
        need it!!)

    -   Enable high precision (should almost always be unchecked) -- if
        checked it will give a higher level of precision (up to 39
        decimal places, likely unnecessary)

    -   Tracing level

-   Identify bottleneck, try-

    -   Outputting data to a flat file instead of the DB as a test

        -   If the session takes way less time than benchmark, then its
            likely a writer bottleneck

        -   Do this by editing session \> mapping \> target, set writer
            to file writer instead of relational writer

        -   If there is very little distance, then you likely do not
            have a writer bottleneck

    -   Test for a read bottleneck now!

        -   Run the session to do nothing but read data

        -   Do this by adding a filter transformation just downstream of
            the SQ and set the filter condition to false (filters
            everything out)

            -   Be sure to refresh the mapping before running the
                session!

        -   This shows you the diff between the whole session running
            and just the read portion

    -   Testing for an engine/mapping bottleneck

        -   There isn't a test, but if you do the other 2 tests and they
            show no issue with read/write as bottleneck, you know you
            have an engine/mapping bottleneck

        -   You can also run the session and creep on the Source/target
            statistics in the session properties via workflow monitor,
            and if the read stops, then takes forever before write
            begins, that confirms

-   Solution for target/write bottleneck?

    -   DBA side

        -   Might need to have DBA write stored procedure to drop
            indexes, load table, then re-index

        -   Might need to introduce database partitioning

        -   Might need to relocate the database to faster hardware

        -   Might consider using an external loader system on the
            database

    -   PWC side

        -   Increase the commit interval

-   Solution for source/read bottleneck?

    -   DBA side

        -   Move IS and source DB closer ?

        -   Increase network speed

        -   Place indexes on source table

-   Solution for mapping/engine bottleneck?

    -   How to know which transformation is the issue?

        -   Any transformation which processes 1 record at a time is
            likely NOT the culprit

        -   Lookup, joiner, aggregator and sorter are likely to be
            sources of a mapping bottleneck

        -   Can use a false filter, locate it anywhere in the mapping,
            and that can help isolate transformations (but this takes a
            lot of time, so NOT recommended as the best way to do this)

        -   BETTER to activate session level performance counts (Check
            "Collect performance data" in the session properties)

            -   Can view these in run properties \> Performance

**[Module 13: Performance Tuning Mapping Design]{.underline}**

-   Remove unnecessary ports (they are overhead for PWC)

    -   Fewer ports = better performance and lower memory requirements

-   Reduce rows in pipeline

    -   Place filter transformation as far upstream as possible! (why
        process a row you're eventually going to drop? Drop it ASAP)

    -   Filter before aggregator, rank or sorter if possible

-   Source Qualifier- only output needed ports!

    -   The SQL select statement the SQ generates will select only those
        columns that are output from the SQ

    -   Still good to keep the ports in the SQ and not delete them, in
        case they need to be used later on to modify the mapping, etc.
        (just don't output them)

-   If you want to join tables and it's possible to do that in the DB,
    do it in the DB instead of using a joiner transformation in PWC

    -   PWC can't do it as efficiently, DBs are designed to join
        efficiently and it's better to do it there if possible

-   When using a joiner transformation,

    -   Need to indicate which ports are the "master" ports

    -   Master ports are what get cached

    -   Better to choose the side of join (which table/source) that is
        smaller/has less data as the master to less is cached, more
        efficient

-   Joiner and Aggregator transformation -- Sorted Input checkbox

    -   Tells the transformations that both pipelines coming in have
        been sorted, which allows the transformation to avoid caching

    -   DON'T want to put a sorter before a joiner/aggregator, though,
        it would likely worsen performance and consume a lot of RAM to
        run the sorter upstream

    -   Check this box if and when the data has been pre-sorted, before
        the session even runs

    -   Might consider creating a session to run before that sorts the
        data and loads into a flat file on the same machine running the
        IS, that way any subsequent sessions that contain
        joiners/aggregators can use the sorted input file

**[Module 14: Memory Optimization]{.underline}**

-   ![](media/image11.png){width="2.5884951881014873in"
    height="1.625in"}

-   Transformation cache does not use the DTM buffer pool --
    transformations have their own memory blocks for caching

-   Can change default values/settings for the DTM buffer, but not
    recommended as PWC is good at setting these things appropriately

    -   Edit Session \> Config Objects \> "default buffer block size" \>
        Auto is default (PWC will decide)

    -   ­­Edit Session \> Properties \> "DTM buffer size" \< this is the
        total pool size

    -   DON'T CHANGE THESE UNLESS YOU HAVE TO

-   When might you want to change this?

    -   When you have really "wide" data

        -   OK so PWC buffer blocks work most efficiently when \~100
            records fit into each block

        -   Each block has \~64KB

        -   SO if you have a situation where you are only fitting \~10
            records in each block, so each record is around \~7KB, this
            might be a situation in which you'd want to set the buffer
            block size to be LARGER so that the ideal \~100 records can
            fit in each block

        -   If you modify the block size, be sure to modify the pool
            size too so that you don't have fewer blocks

    -   

-   If PWC runs out of buffer blocks, the session won't fail, the
    threads will just be on "hold" until a buffer block frees up

-   How do you know if a transformation is using the RAM vs. the
    transformation caches ??

    -   When you run, look at session properties \> Performance (have to
        turn this tracking on \> "collect performance data")

    -   There are various counts

        -   Readfromcache / writetocache

        -   Readfromdisk / writetodisk

    -   Some transformations may have counts for all 4, but some may
        not. If a transformation only has counts for the cache and not
        the disk, then no RAM is being used and they will NOT benefit
        from any sort of memory optimization

-   Can also adjust the cache sizes

    -   Edit session \> mapping \> choose transformation \> aggregator
        index cache size / aggregator data cache size

    -   Can override the Auto with an actual amount of RAM you wish to
        allocate OR

    -   Can go to edit session \> config object and set the maximum
        percent or maximum absolute value of memory to be allocated for
        X

**[Module 15: Performance Tuning: Pipeline Partitioning]{.underline}**

-   Partitioning allows you to run a single logical session as a
    collection of physical sessions and possibly run those physical
    sessions on different machines

    -   Each individual partition processes a subset of the data

    -   Enables hard drives performance to be optimized

-   This would be AFTER applying all other performance tuning methods
    (above \^)- it should not be used as a way of avoiding those other
    things

-   Partition = subset of the data

-   Stage = a portion of a pipeline

-   Partition point = boundary between two stages

-   Partition type = algorithm for distributing data among partition;
    always associated with a partition point

-   ![](media/image12.png){width="6.5in" height="2.759027777777778in"}

-   The DTM implements each of those stages as a thread, so they run in
    parallel! You can add or remove the partition points

-   Rules for adding partition points

    -   You cannot add a partition point to a sequence generator

    -   You cannot add a partition point to an unconnected
        transformation

    -   You cannot add a partition point on a source definition

    -   If a pipeline is split and then concatenated, you cannot add a
        partition point on any transformation between the split and
        concatenation

    -   Adding or removing partition points requires the partitioning
        license

-   Partitioning will only give you a benefit if you have good hardware
    and a fast network, otherwise there will be little to no improvement
    in performance
