**<span class="underline">Module 1: PowerCenter Overview</span>**

  - PowerCenter is “database neutral” meaning it will communicate with
    any database

  - PowerCenter provides a unified view of data to integrate different
    data sources into one data warehouse

  - How does it do this? With ETL methodology (extract, transform, load)

  - Many other Informatica tools can be used along with PowerCenter,
    such as…
    
      - Can use Informatica Developer Tool to cleanse the data before
        putting it into PowerCenter
    
      - Can also use MDM on the exports from PowerCenter before putting
        that data into the data warehouse

  - **ETL** in PowerCenter
    
      - 1\. PC reads data row by row from a table/file/etc. The
        structure of the source is contained in a source definition
        object
    
      - 2\. Data is transformed using transformation objects
    
      - 3\. PC writes the data row by row to a target table, structure
        of the target table is called the target definition

  - **Mappings** define the ETL process and must include at least one
    source definition and at least one target definition (contains a set
    of transformations)

  - PC **Workflows** are objects that can be run- they are an ordered
    set of one or more sessions and other tasks. Can run sequentially,
    concurrently, and can include conditions upon which to run or not
    run

  - PC **Architecture**
    
      - Repository and Integration services together make up a
        PowerCenter domain
    
      - Administrator interacts with the domain (via HTPPS) as an
        entity. Administrator is where security controls like
        permissions/user groups happens
    
      - PC Repository Manager, Workflow Manager, etc. interact with the
        domain via TCP/IP requests
    
      - Sources interact with the domain via native drivers/ODBC
    
      - Sources interact with PC repository manager, etc. vai ODBC
    
      - ![](./media/image1.png)

**<span class="underline">Module 2: ETL Fundamentals</span>**

  - PC Designer is used to design source definitions, target
    definitions, transformations, mappings, etc.

  - **PC Designer** – Transformations
    
      - Source Qualifier – reads form data sources
    
      - Expression – performs row-level calculations
    
      - Filter – uses a condition statement to select rows for inclusion
    
      - Sorter – sorts data
    
      - Aggregator – performs aggregate calculations
    
      - Joiner – joins heterogeneous sources
    
      - Lookup – looks up values and passes them to other objects
    
      - Router – routes rows conditionally
    
      - Update strategy – tags rows for insert, update

  - Passive vs. active transformation
    
      - Passive – does not change the number of rows (same \# input as
        output)
    
      - Active – does change the number of rows (i.e. discards some rows
        in the transformation)

  - Source Qualifiers
    
      - Helps you read data from the source
    
      - Default is to select all columns (and transform the data type to
        a compatible data type)
    
      - Could change this to select only certain columns

  - Transformation ports in PC designer represent columns

  - Shortcuts
    
      - Dynamic link back to an original object (that must be in the
        Shortcut folder)
    
      - Can create one for any object in the repository

  - **Workflow Manager**
    
      - Used to create workflows which can execute the mappings you make
        in designer as well as other tasks
    
      - Task Manager within this tool allows you to create all kinds of
        taks….
        
          - Session – run the logic of a mapping
        
          - Command – run external commands (i.e. shell scripts)
        
          - Email – send an email to a defined recipient
        
          - Decision – choose between two paths in a workflow
        
          - Assignment – assign values to variables
        
          - Timer – wait or pause
        
          - Control – terminate or fail a workflow
        
          - Event wait – wait for an event
        
          - Event raise – cause an event wait task to trigger
    
      - Can monitor the workflows as they’re happening using the
        Workflow monitor (can also look at logs or results of the
        workflow monitor)

  - “Velocity methodology” – a best practice sort of thing that includes
    templates and naming conventions (i.e. workflows name with “Wf\_”,
    mappings with “M\_” etc.)

**<span class="underline">Module 3: Troubleshooting</span>**

  - Common causes of failed sessions
    
      - Workflow manager – check target table I database
    
      - Designer – clear the fail parent if this task fails checkbox in
        the workflow properties

  - Methodology
    
      - 1\. Get the session run properties for the failed session (right
        click session, get run properties)
    
      - 2\. Check the run properties for the failed session
    
      - 3\. If defined properly, view the failure information window for
        specifics
    
      - 4\. Open and examine the session log for more details
    
      - 5\. If unable to fetch session log, there’s no info so you have
        to look at the workflow log
    
      - 6\. Determine if the PC developer can fix or if it needs to be
        escalated

  - Other options
    
      - Run output data to a flat file to eliminate possible target
        errors
    
      - Use the debugger
    
      - Use the “create and add target” option to see output at that
        point in the mapping

  - Common connection errors
    
      - Connection properties set incorrectly
        
          - Requestor must have permissions to use connection
        
          - If, incorrect connection selected, table not located
        
          - Connections are not set (required for each heterogeneous
            source, target and lookup)
    
      - Typos
        
          - Database password
    
      - Bulk Load type not set to Normal
        
          - This applies to Oracle tables with keys only
        
          - If Bulk is selected and there is an index defined on the
            target table, you’ll get the error “ORA-26002-table has
            index defined upon it”
        
          - Bulk loading doesn’t support indexing

  - Common PowerCenter and User Errors
    
      - Integration or Repository Service not running
    
      - Database unavailable or configured incorrectly
    
      - Lookup variables, $Source / $Target connection value not set
    
      - Wrong session or task run
    
      - Wrong mapping selected for session or mapping deleted
    
      - Source file errors

  - Rejected records
    
      - All targets have a reject file that contains rows of data that
        the writer did not write to the target
    
      - Flagged for reject if it violates database constraints, field in
        the row is truncated or overflowed, etc.
    
      - Reject file is .bad file – examine this
    
      - Examine the run properties and/or session log for records that
        have transformation errors

**<span class="underline">Module 4: PowerCenter Transformations, Tasks
and Reusability</span>**

  - Expression transformation
    
      - Passive
    
      - Performs row-level calculations
    
      - Used for most data manipulation
    
      - Ports- mixed, variables allowed
    
      - No aggregate function

  - Expression Editor (to write expressions)
    
      - Can nest these (evaluated inside out)
    
      - Functions grouped by category
    
      - Pretty self explanatory to use

  - Variable ports
    
      - Store data *temporarily*
    
      - Used to simplify complex expressions, provide storage for
        temporary values, improve efficiency
    
      - Not visible in normal view, only in edit view
    
      - Cannot be input or output ports
    
      - Must select the “V” checkbox to make a variable port

  - Order of evaluation
    
      - 1<sup>st</sup> input and input/output ports
    
      - 2<sup>nd</sup> variable ports
    
      - 3<sup>rd</sup> output ports
    
      - Within each, they are evaluated in order

  - Expression validation
    
      - Can click “Validate” from within the expression editor to
        debug/verify
    
      - Function arguments must be separated by a comma
    
      - Except for literals,
        
          - Transformation is not case sensitive
        
          - Parser and server ignore spaces
    
      - Colon, comma and period are reserved to specify syntax
    
      - A dash (-) is treated as a minus operator
    
      - Any string value enclosed in single quotation marks is treated
        as a character string
    
      - String values for TO\_CHAR and GET\_DATE\_PART are not checked
        for validity
    
      - Nested expressions are evaluated starting with the innermost
        expression

  - Filter transformations are processed on a row by row basis

  - File lists
    
      - List of flat files or xml files
    
      - Allows you to process multiple files sequentially

  - Reusable transformations- can reuse transformations for many
    different mappings
    
      - Can promote a non-reusable transformation to a reusable one
    
      - Can create one first using the transformation developer

  - Email functionality
    
      - Must configure the integration service to send mail
    
      - Then you can….
        
          - Configure sessions to send post-session email
        
          - Configure workflows to send suspension email
        
          - Create email tasks within the workflow

  - NOTE: Do not use the CONCAT() function, instead use the || to
    concatenate. It’s faster.

  - NOTE: Velocity best practice is to prefix input only ports with
    “in\_” and output only ports with “out\_”

  - NOTE: When using a file list as input into Workflow Manager, be sure
    to set the source filetype to INDIRECT (from Direct)

**<span class="underline">Module 5: Features and Techniques</span>**

  - Arrange mappings feature (right click mappings screen, arrange all)
    
      - Can also arrange iconic

  - Autolink ports – Options to autolink by position or name

  - Change port order in transformations – there are up and down errors
    in the top right of the ports window that allow you to move their
    position

  - Can switch between transformations while editing using the drop down
    menu

  - To see everything on the screen, adjust the zoom OR press the handy
    little “scale” button next to the zoom option

**<span class="underline">Module 6: Joins and Link Conditions</span>**

  - Two types of joins in PC- heterogeneous and homogeneous

  - Source qualifiers can perform homogeneous joins

  - Specify user defined joins in the Properties tab of the edit
    transformation window
    
      - Default is inner join
    
      - Can override the SQL query to change the join type

  - Homogeneous join demo
    
      - 1\. Drag tables to source analyzer window (from sources- ORAC,
        or whatever database schema youre using)
    
      - 2\. In Target designer, create new target tables- drag table and
        rename
    
      - 3\. Edit the table, in Columns tab give the fields and
        properties (data type, keys, etc.)
    
      - 4\. Targets \> Generate/Execute SQL \> generate
    
      - 5\. In Mapping designer, create new mapping and drag the tables
        in (this will create SQ for both)
    
      - 6\. Delete the SQ for one table, drag those ports to the SQ for
        the other table (this will use the single SQ left as the join)
    
      - 7\. Double click SQ, go to Properties, define the SQL condition
        upon which to join

  - \*\*\*If source tables are from the same database and have a key
    relationship
    
      - Only a single source qualifier is necessary

  - Heterogeneous Join
    
      - Joins using dissimilar sources (i.e. Oracle table and flat file)
    
      - Must use a joiner transformation in the mapping to do this
    
      - You’ll have an SQ for each source, then use the joiner
        transformation on those SQs

  - Joiner transformation
    
      - Active transformation
    
      - One source is designated as the master, the other the detail
    
      - M property indicates ports from the Master source when checked

  - Heterogeneous join demo
    
      - 1\. From Designer, drag the sources into the source analyzer
        (tables, files, whatever)
    
      - 2\. Go to Target designer and create the target table or select
        the target if it already exists
    
      - 3\. Generate and execute SQL from the target tab on the toolbar
    
      - 4\. Mapping designer- create new mapping, drag the sources in
    
      - 5\. Select the joiner transformation icon
    
      - 6\. Drag the columns from both SQs into the transformation
    
      - 7\. Edit transformation – delete ports not required, select
        which table is the master table (select the table with the least
        number of ports – the joiner transform will compare each of the
        detail source against the master)
    
      - 8\. Go to the conditions tab to create the condition on which to
        join
    
      - 9\. Drag the target and connect the Joiner to the target

  - Join types
    
      - Normal (inner) join keeps only matching rows
    
      - Master outer join keeps all rows form master, matching from
        detail
    
      - Detail outer join keeps all rows from detail, matching from
        master
    
      - Full outer join keeps all rows from all

  - Joiner Cache
    
      - Two types: index and data
    
      - All rows from master source are read into the cache
    
      - Index cache contains values from the ports which are part of the
        join condition
    
      - Data cache contains values from all ports not specified in the
        join condition

  - Link Conditions demo
    
      - 1\. Workflow manager, create workflow, create a session for each
        join type (hetero and homo)
    
      - 2\. Link them
    
      - 3\. Edit the session tasks, go to mappings and set source
        connection type to scott, set target connection type as TGT
    
      - 4\. Double click the link between the two sessions to edit. Get
        the status function. Set status = succeeded (this means it will
        only continue to the next session if the first session completes
        successfully)

  - Workflow link conditions
    
      - To set a condition, right click a link and enter an expression
        that evaluates to True or false
        
          - If the condition is true, next task is executed (if false,
            its not)

  - Incoming links
    
      - Under the general tab of each task you can use the radio buttons
        under “treat the input links as” and choose AND or OR
        
          - OR – run the task as soon as any link condition is true
        
          - AND – run the task when all link conditions are true

**<span class="underline">Module 7: Using the Debugger</span>**

  - Debugger allows you to…
    
      - Follow a record across a mapping from transformation to
        transformation
    
      - Set and modify breakpoints
    
      - Change data and variable values

  - Debugger uses a session to run the mapping on an Integration
    service, and it will pause at breakpoints so you can view/edit data

  - Debugger Interface demo
    
      - Open the mapping, click Mappings \> Debugger \> Start Debugger
    
      - Transformation instance window shows you data at each breakpoint
        for each transformation instance (use dropdown to choose).
    
      - Target Instance window shows you what will happen to a record
        (update/delete/etc.), use the dropdown to select between
        multiple targets.
    
      - Mapping window shows the step by step execution path of a
        mapping and highlights the step being executed.

**<span class="underline">Module 8: Sequence Generators, Lookups and
Additional Workflow Tasks</span>**

  - Lookup is a transformation to look up the values from a relational
    table/view or flat file

  - Returns values from a database table or flat file associated with a
    given input value

  - Lookup components
    
      - Lookup source – can be a flat file or relational table
    
      - Lookup types – connected, unconnected
        
          - Connected – receives input values directly from mapping
            pipeline; commonly used when the look is needed for every
            record
        
          - Unconnected – commonly used when you do not need to examine
            every record; receives values from LKP expression from
            another transformation
    
      - Ports – input, output, Look up, return
        
          - Input -
        
          - Output – ports going to another transformation from the
            lookup
        
          - Look up ports – columns of data to return from the lookup
            source
        
          - Return ports –
    
      - Lookup condition – compares one or more input fields with fields
        in the lookup source; like a WHERE clause in SQL; values that
        match the condition are returned

  - Lookup policy on multiple match – determines what happens when
    lookup transformation finds multiple rows that match the lookup
    condition
    
      - When you indicate Use First/Use Last, SQL sorting is used to
        accomplish this (order by)

  - Simple lookup demo:
    
      - Flat file source (employee data)
    
      - Looking up the department name in a department table using the
        department number from the employee data (dept.dept num =
        emp.dept num)
    
      - 1\. In Designer/Source analyzer, import source
    
      - 2\. Import target from Designer/Target Designer
    
      - 3\. Import lookup table (departments) as a target from Target
        Designer
    
      - 4\. Create your mapping – drag the employee source file, the
        employee target definition
    
      - 5\. Create a transformation expression, connect all SQ employee
        flat file ports to the expression transformation
    
      - 6\. Create a lookup transformation – connect dept No. from
        expression transformation to lookup transformation. Specify dept
        no = dept no condition.
    
      - 7\. Connect all ports other than dept no. from expression
        transformation to target
    
      - 8\. Connect department name and number from lookup
        transformation to the target
    
      - 9\. Create and run the workflow\!

  - Treat source row as…
    
      - Insert (default) -
    
      - Update
    
      - Delete
    
      - Data-driven

  - Event wait task – pauses processing of the pipeline until a
    specified event occurs
    
      - Can be pre-defined ( a file watch )
    
      - Or user defined

  - Timer task – relative or absolute time

  - Email task – send an email if \_\_\_ when \_\_\_\_ etc.
    
      - Integration service must be configured to send an email

  - Control task – can stop, fail or abort the workflow

  - Control options
    
      - ![](./media/image2.png)

  - Command task – specifies one or more shell commands to run during a
    workflow (UNIX, DOS, etc.)
    
      - Can also run shell commands pre or post session

  - Decision task – tests for a condition during the workflow and sets a
    flag based on the condition
    
      - Use a link condition downstream to test the flag and control
        execution flow (or a control task)

  - Sequence Generator Transformation
    
      - Passive
    
      - Generates unique keys for any port on a row
    
      - Two predefined output ports, cannot edit or delete these
        
          - NEXTVAL
        
          - CURRVAL
    
      - Used to generate sequenced keys, replace missing primary keys,
        share across mappings
    
      - ![](./media/image3.png)

  - Sequence Generator properties
    
      - Start value – to set start value for keys (default is 0)
    
      - If you select cycle, it cycles back to start value when it
        reaches the maximum value
    
      - End value is the maximum value that PC generates
    
      - Increment by – difference between two consecutive values
    
      - Current value – what you want PC to use as the first value in
        the sequence
    
      - If reset is enabled, PC server generates values based on the
        current value for each session

  - Lookup Cache
    
      - Uses index and data cache
        
          - Index cache stores data for the columns used in the lookup
            condition
        
          - For connected lookup data cache contains values from all
            connected output/lookup ports which are not part of the
            lookup condition
        
          - For an unconnected Lookup transformation Data cache stores
            data from the return port
    
      - After the cache is loaded, values from the Lookup input ports
        that are part of the lookup condition are compared to the index
        cache
        
          - When a match is found, the rows from the cache are included
            in the stream
    
      - Using the lookup cache can increase the session performance
        (especially when the number of lookup rows is much higher than
        the number of lookup values)

  - Lookup Cache Types
    
      - Static – cannot insert or update the cache
    
      - Dynamic – can insert or update when you pass the rows
    
      - Persistent – will save the lookups (?)
    
      - Recache from source – can rebuild the lookup caches

  - Lookup Caching Properties
    
      - Edit Transformations \> Properties \> check box for “lookup
        caching enabled”
    
      - Edit Transformations \> Properties \> Lookup cache directory
        name – fill in variable
    
      - Edit Transformations \> Properties \> Lookup cache persistent
        checkbox for persistent cache
        
          - Persistent cache can improve performance but may pose a
            stale data problem
    
      - Edit Transformations \> Properties \> Re-cache from lookup
        source checkbox for that type

**<span class="underline">Module 9: Sorting and Aggregating Data Using
PowerCenter</span>**

  - Update Strategy Transformation
    
      - Specifies how each row updates target tables
    
      - Insert/update/delete/reject based on an expression
    
      - Active transformation
    
      - All ports are input/output
    
      - Can specify the update strategy expression (IIF or DECODE logic
        determines how to handle the record)

  - Update Strategy Flags
    
      - DD\_INSERT – row is tagged for insertion into the target table
    
      - DD\_UPDATE – row is tagged for update
    
      - DD\_DELETE – row is tagged for deletion from target table
    
      - DD\_REJECT – row is tagged for rejection and no action is taken
        on the target table
    
      - These flags are not additional fields and cannot be modified by
        later transformations

  - Rejected records
    
      - By default the Integration Service process creates a reject file
        for each target
    
      - These files contain all the rejected rows not inserted/updated
        in the target
    
      - To find these, can examine this .bad file or examine the session
        run properties for \# of rejected records

  - Router Transformation
    
      - This transformation sends data rows to different destinations
        based on filter conditions
    
      - Active transformation with input and output ports
    
      - Can be better than cases depending on what you want
        
          - Cases – once a case is true, the record falls out and goes
            to the associated destination
        
          - Router – a record can be true for multiple “routes” and will
            end up in each destination that is “true” for it; can end up
            in multiple targets
        
          - Additionally, there is a default target that “catches” any
            records that didn’t go to another target

  - Router Groups
    
      - Input group (always one)
    
      - User-defined output groups
        
          - Each group has one condition
        
          - ALL group conditions are evaluated for each row
        
          - One row can pass multiple conditions
    
      - Make group conditions mutually exclusive to avoid this if you
        want ^
    
      - Unlinked group outputs are ignored
    
      - Default group (always one) captures rows that fail all group
        conditions

  - Port default values
    
      - These determine how PC handles NULL (all ports have defaults)
    
      - User-defined defaults are allowed for some transformations to
        override the system defaults
        
          - For input and I/O ports, default values are used to replace
            null values
        
          - For output ports, default values are used to handle
            transformation calculation errors (not-null handling)
    
      - At the bottom of the ports tab in “Edit transformation”, when a
        port is selected, there is a box at the bottom of the window for
        the default value – defaults must match the datatype of the port

  - Source Qualifier Overrides
    
      - Several options exist for overriding the SQL queries generated
        by PC
    
      - Do this on the properties tab of the edit transformations
        window,
        
          - “Sql query” box to edit the SELECT portion
        
          - “user defined join” box to edit the WHERE clause
        
          - “source filter” for a modified WHERE
        
          - “number of sorted ports” for the ORDER BY piece
        
          - “Select distinct” to add DISTINCT to the query

  - Target update override
    
      - By default, target tables are updated based on key values
    
      - Can change this in target properties by…
        
          - Update Override
        
          - Generate SQL
        
          - Edit the statement

  - Session task mapping overrides –
    
      - Can override some SQ attributes in the session task mapping tab
    
      - Can also override some target attributes in the session task
        mapping tab

**<span class="underline">Module 10: Sorting and Aggregating Data Using
PowerCenter</span>**

  - Sorter Transformation
    
      - Active transformation that sorts data in ascending or descending
        order according to a sort key
    
      - When you check “key” checkbox next to the ports tab in “Edit
        transformation”, the key is used to sort
    
      - Next to the port checked off as “key”, indicate the direction
    
      - The order of the ports will indicate which sort is done first

  - Sorter Transformation Properties
    
      - “Sorter cache size” – can set this\*
    
      - “Case sensitive” – can indicate to sort case sensitive
    
      - “Distinct output rows” – mapping designer configures all the
        ports as part of the sort key and discards duplicate rows after
        this transformation (this feature is what makes the
        transformation active)
    
      - “Null treated low” – treats NULL values as lower than all other
        values

  - \*Sorter Cache
    
      - All incoming data is passed into sorter cache memory before the
        sorting is done
    
      - Can set the size of the cache (above)
    
      - If the cache is larger than the available amount of memory, the
        session fails
    
      - If the amount of incoming data is greater than the cache size,
        integration service temporarily stores data in the sorter
        transformation work directory

  - Aggregator Transformation
    
      - Performs aggregate calculations (i.e. average, sum)
    
      - Active
    
      - Can create aggregate expressions in on-input ports
    
      - Can do “group by”
        
          - Check box in the ports tab

  - Aggregator Properties
    
      - “Sorted input” – improves aggregator performance; aggregator
        assumes all data is sorted by a group when this is checked so it
        reads in data row by row assuming its in grouped order
        
          - \*WHEN THIS IS USED YOU MUST PASS SORTED DATA TO THE
            TRANSFORMATION
        
          - If you don’t, the session will fail
    
      - “Tracing Level” – amount of detail included in the log

  - Aggregator Cache
    
      - Two types – index and data
    
      - Can set these sizes in the properties tab
    
      - Index contains group by port values
    
      - Data contains all port values, including variable and connected
        output ports
    
      - Data cache is typically larger than index cache

  - Aggregate Expressions
    
      - Can create these using aggregate functions
    
      - Conditional expressions are supported

  - Data Concatenation
    
      - Determines whether some ports (data flow arrows) can bypass a
        transformation
    
      - Works only if
        
          - Combining branches of the same source pipeline
        
          - AND neither branch contains an active transformation

  - Unconnected Lookup
    
      - Commonly used when a Lookup not needed for every record
    
      - No links to/from other transformations
    
      - lookup data is called at the point in the mapping that needs it
    
      - Lookup function can be set within any transformation that
        supports expressions

  - Unconnected Lookup Functionality
    
      - Used when not all rows need the lookup – you are looking up
        something that only affects some of the rows from the source:
        
          - ![](./media/image4.png)
    
      - *One* lookup port value may be returned for each lookup
    
      - The omission of a selected Return Port will not make the mapping
        invalid, however the lookup returns all the nulls

  - Calling the unconnected lookup
    
      - Use lookup function within a conditional statement
    
      - Condition is evaluated for each row but Lookup function is
        called only if condition is satisfied
    
      - ![](./media/image5.png)

  - Unconnected lookup key points
    
      - An unconnected lookup can improve performance because it is only
        called when the lookup condition is satisfied
    
      - Use the lookup with a conditional statement
        
          - The condition is evaluated only for those rows where the
            condition evaluates to TRUE
    
      - The transformation is called using the expression
        :lkp.lookupname
    
      - Data from several input ports may be passed to the Lookup
        transformation but only one port may be returned

  - ![](./media/image6.png)

  - ![](./media/image7.png)

  - System Variables (some examples, there are many)
    
      - SYSDATE – provides current datetime on the PowerCenter Server
        machine
        
          - Not a static value
    
      - SESSSTARTTIME – returns the system date value on the PowerCenter
        server
        
          - Used with any function that accepts transformation date/time
            datatypes
        
          - Not to be used in a SQL override
        
          - Has a constant value
    
      - $$$SessStartTime – returns the system date value as a string.
        Uses system clock on machine hosting PowerCenter server.
        
          - Format of the string is database type dependent
        
          - Used in SQL override
        
          - Has a constant value

  - Can use system variables in an expression by using the expression
    editor, clicking the variables tab and looking under the “Built in”
    folder

  - Mapping parameters and variables
    
      - Represent declared values
        
          - Variables can change in value during run-time
        
          - Parameters remain constant during run-time
    
      - Declared in Mappings menu
        
          - Format is $$VariableName or $$ParameterName
    
      - Value is initialized by the specification that defines it
    
      - Can be used in pre and post SQL
    
      - Increases the flexibility and reusability of a mapping
    
      - Apply to all transformations within a mapping

  - Declared variables set by….
    
      - 1\. Value in the parameters file
    
      - 2\. If no file, can indicate an “initial value”
    
      - 3\. If no initial, there are defaults based on datatype

  - Functions to set mapping variables
    
      - SETVARIABLE($$Variable, value) – sets the variable to the value
        specified
    
      - SETCOUNTVARIABLE($$Variable) – increments a counter variable
    
      - SETMAXVARIALBE($$Variable, value) – sets the variable to the
        larger of its current value and the value passed to the function
    
      - SETMINVARIABLE($$Variable, value) – sets the variable to the
        smaller of its current value and the value passed to the
        function

  - Parameter file
    
      - Can be used to set values of mapping parameters and variables
    
      - Can associate parameters with a service or service process such
        that they apply to any workflow or session running on that
        service or service process
    
      - Can be applied to
        
          - Individual session task properties
        
          - An entire workflow (and all sessions within it)
        
          - Or each session task and/or workflow may have its own
            parameter file or parameter file section
    
      - To indicate the file for a workflow, edit the workflow,
        Properties tab, indicate the parameter filename
        
          - To then use these parameters, edit the session within the
            workflow, Mappings tab, for various connections, etc. use
            these variables instead of hard coding values
    
      - Can also be specified using the pmcmd command when starting a
        session task

![](./media/image8.png)

  - Mapplet
    
      - Reusable mappings that contain a set of transformations and lets
        your reuse the transformation logic in multiple mappings
    
      - Can incorporate these mapplets into a mapping for a portion of
        the logic to make life easier
    
      - When you use the mapplet in a mapping, you are using an instance
        of the mapplet
    
      - Advantages
        
          - Useful for repetitive tasks
        
          - Represent a set of transformations
        
          - Changes are inherited by all instances
        
          - PowerCenter server expands the mapplet at runtime
    
      - Created in mapplet designer (must configure for input and output
        to use in a mapping)
    
      - Mapplet Input Transformation
        
          - Passive
        
          - Output ports only
        
          - Only those ports connected from an input transformation to
            another transformation will display in the resulting mapplet
        
          - Cannot connect a single port to multiple transformations
    
      - Mapplet Output Transformation
        
          - Passive
        
          - Input ports only
        
          - Only those ports connected to an output transformation will
            display in the resulting mapplet
        
          - One or more mapplet output transformations are required in
            every mapplet
    
      - Can create a mapplet with multiple output groups
    
      - Can also output multiple instances of the same target table
    
      - Mapplets can be active or passive depending on the
        transformations contained within it
        
          - Multiple passive mapplets can populate the same target
            instance
        
          - Multiple active mapplets or active and passive mapplets
            cannot populate the same target instance
    
      - Mapplet parameters and variables
        
          - Similar to mapping parameters and variables
        
          - Defined using Mapplets \> parameters and variables
        
          - A parameter or variable defined in a mapplet is not visible
            in any parent or child mapplet
        
          - To use these, cache them as input port
    
      - NOTE: Parameters/variables created for mappings cannot be used
        in mapplets

  - Multiple Row Return Lookups
    
      - Configure the lookup transformation to return all matching rows
    
      - The lookup transformation returns all rows that match the lookup
        condition
    
      - Must select “Return all values on multiple match” checkbox when
        creating lookup to do this
    
      - The lookup policy on multiple match values….
        
          - Will default to use all values
        
          - Will become read only
        
          - You cannot change the property after you create the
            transformation
    
      - Lookup becomes active when you do this
    
      -
